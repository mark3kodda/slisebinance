package ua.com;

import javax.websocket.*;
import java.util.ArrayList;
import java.util.List;

@ClientEndpoint
public class MyClientEndpoint {

    private static final JSONParser json = new JSONParser();

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Connected to endpoint: " + session.getBasicRemote());

    }

    @OnMessage
    public void processMessage(String message) {
        System.out.println("Received message in client: " + message);
        B data = json.parseFromJSON(message, B.class);
        Cache.listData.add(data);
        Client.messageLatch.countDown();
    }

    @OnError
    public void processError(Throwable t) {
        t.printStackTrace();
    }


}