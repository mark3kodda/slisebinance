package ua.com;

import javax.websocket.DeploymentException;
import javax.websocket.ContainerProvider;
import javax.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class Client {

    public static final String URL_BIN =
            "wss://stream.binance.com:9443/stream?streams=bnbbtc@bookTicker/ethbtc@bookTicker/xrpeth@bookTicker/bnbusdt@bookTicker/ltcbtc@bookTicker";

    final static CountDownLatch messageLatch = new CountDownLatch(10);

    public static void main(String[] args) {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            while (true){
            container.connectToServer(MyClientEndpoint.class, URI.create(URL_BIN));

            }
        } catch (IOException | DeploymentException ex) {
            ex.printStackTrace();
        }
    }
}
