package ua.com;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JSONParser {


    public <T> T parseFromJSON(String json, Class<T> targetClass) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return  objectMapper.readValue(json, targetClass);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
