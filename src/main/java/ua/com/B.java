package ua.com;

import java.util.Map;

public class B {
    private String stream;
    private CriptDTO data;

    public B(String stream, CriptDTO data) {
        this.stream = stream;
        this.data = data;
    }

    public B() {
    }

    public String getStream() {
        return stream;
    }

    public void setStream(String stream) {
        this.stream = stream;
    }

    public CriptDTO getData() {
        return data;
    }

    public void setData(CriptDTO data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "B{" +
                "stream='" + stream + '\'' +
                ", data=" + data +
                '}';
    }
}
