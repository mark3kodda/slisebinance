package ua.com;

public class CriptDTO {

    private long u;
    private String s;
    private float b;
    private float B;
    private float a;
    private float A;

    public long getU() {
        return u;
    }

    public void setU(long u) {
        this.u = u;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public float getB() {
        return b;
    }

    public void setB(float b) {
        this.b = b;
    }

    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }

    @Override
    public String toString() {
        return "CriptDTO{" +
                "u=" + u +
                ", s='" + s + '\'' +
                ", b='" + b + '\'' +
                ", B='" + B + '\'' +
                ", a='" + a + '\'' +
                ", A='" + A + '\'' +
                '}';
    }

}
