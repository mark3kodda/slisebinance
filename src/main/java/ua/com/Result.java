package ua.com;

import java.util.ArrayList;
import java.util.List;

public class Result {

    private long averageAsk;
    private long mediumAsk;
    private long minAsk;
    private long maxAsk;
    private long averageBin;
    private long mediumBin;
    private long minBin;
    private long maxBin;

    public long getAverageAsk() {
        return averageAsk;
    }

    public void setAverageAsk(long averageAsk) {
        this.averageAsk = averageAsk;
    }

    public long getMediumAsk() {
        return mediumAsk;
    }

    public void setMediumAsk(long mediumAsk) {
        this.mediumAsk = mediumAsk;
    }

    public long getMinAsk() {
        return minAsk;
    }

    public void setMinAsk(long minAsk) {
        this.minAsk = minAsk;
    }

    public long getMaxAsk() {
        return maxAsk;
    }

    public void setMaxAsk(long maxAsk) {
        this.maxAsk = maxAsk;
    }

    public long getAverageBin() {
        return averageBin;
    }

    public void setAverageBin(long averageBin) {
        this.averageBin = averageBin;
    }

    public long getMediumBin() {
        return mediumBin;
    }

    public void setMediumBin(long mediumBin) {
        this.mediumBin = mediumBin;
    }

    public long getMinBin() {
        return minBin;
    }

    public void setMinBin(long minBin) {
        this.minBin = minBin;
    }

    public long getMaxBin() {
        return maxBin;
    }

    public void setMaxBin(long maxBin) {
        this.maxBin = maxBin;
    }

    public Result(long averageAsk, long mediumAsk, long minAsk, long maxAsk, long averageBin, long mediumBin, long minBin, long maxBin) {
        this.averageAsk = averageAsk;
        this.mediumAsk = mediumAsk;
        this.minAsk = minAsk;
        this.maxAsk = maxAsk;
        this.averageBin = averageBin;
        this.mediumBin = mediumBin;
        this.minBin = minBin;
        this.maxBin = maxBin;
    }
}
